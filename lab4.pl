

% Define initial state as number of M and C in start margin Me and Ce in end margin and B, boat position.
% state(M, C, Me, Ce, B).
% state(3, 3, 0, 0, s) -> [state(2, 3, 1, 0, e), state(3, 2, 0, 1, e)]

adjacent_mc(state(M, C, Me, Ce, s), [A, B, CC, D, E]):-
	M1 is M-1,
	Me1 is Me+1,
	C1 is C-1,
	Ce1 is Ce+1,
	M2 is M-2,
	Me2 is Me+2,
	C2 is C-2,
	Ce2 is Ce+2,
	% take one missionary
	A = state(M1, C, Me1, Ce, e),
	% take one cannibal
	B = state(M, C1, Me, Ce1, e),
	% take two missionaries
	CC = state(M2, C, Me2, Ce, e),
	% take two cannibals
	D = state(M, C2, Me, Ce2, e),
	% take of of each
	E = state(M1, C1, Me1, Ce1, e).

adjacent_mc(state(M, C, Me, Ce, e), [A, B, CC, D, E]):-
	M1 is M+1,
	Me1 is Me-1,
	C1 is C+1,
	Ce1 is Ce-1,
	M2 is M+2,
	Me2 is Me-2,
	C2 is C+2,
	Ce2 is Ce-2,
	% take one missionary
	A = state(M1, C, Me1, Ce, s),
	% take one cannibal
	B = state(M, C1, Me, Ce1, s),
	% take two missionaries
	CC = state(M2, C, Me2, Ce, s),
	% take two cannibals
	D = state(M, C2, Me, Ce2, s),
	% take of of each
	E = state(M1, C1, Me1, Ce1, s).


is_valid(state(_, _, Me, Ce, s)):-
	Me >= 0, Me =< 3,
	Ce >= 0, Ce =< 3,
	Me >= Ce.
is_valid(state(_, _, 0, Ce, s)):-
	Ce >= 0, Ce =< 3.
is_valid(state(M, C, _, _, e)):-
	M >= 0, M =< 3,
	C >= 0, C =< 3,
	M >= C.
is_valid(state(0, C, _, _, e)):-
	C >= 0, C =< 3.


add_to_queue_if_valid_and_unvisited([], _, Queue, Queue).
add_to_queue_if_valid_and_unvisited([X|Tail], Parents, Queue, [[X|Parents]|NewQueue]):-
	is_valid(X),
	not(member(X, Parents)),
	add_to_queue_if_valid_and_unvisited(Tail, Parents, Queue, NewQueue).
add_to_queue_if_valid_and_unvisited([_|Tail], Parents, Queue, NewQueue):-
	add_to_queue_if_valid_and_unvisited(Tail, Parents, Queue, NewQueue).


sub_dfs([X|_], Goal, X):-
	X = [Goal|_].
sub_dfs([X|Queue], Goal, Result):-
	X = [Y|_],
	adjacent_mc(Y, Adjacent),
	add_to_queue_if_valid_and_unvisited(Adjacent, X, Queue, NewQueue),
	sub_dfs(NewQueue, Goal, Result).


append_to_queue_if_valid_and_unvisited([], _, Queue, Queue).
append_to_queue_if_valid_and_unvisited([X|Tail], Parents, Queue, RQueue):-
	is_valid(X),
	not(member(X, Parents)),
	append_to_queue_if_valid_and_unvisited(Tail, Parents, Queue, NewQueue),
	append(NewQueue, [[X|Parents]], RQueue).
append_to_queue_if_valid_and_unvisited([_|Tail], Parents, Queue, NewQueue):-
	append_to_queue_if_valid_and_unvisited(Tail, Parents, Queue, NewQueue).
	


sub_bfs([X|_], Goal, X):-
	X = [Goal|_].
sub_bfs([X|Queue], Goal, Result):-
	X = [Y|_],
	adjacent_mc(Y, Adjacent),
	append_to_queue_if_valid_and_unvisited(Adjacent, X, Queue, NewQueue),
	sub_bfs(NewQueue, Goal, Result).


% dfs_algorithm(state(3,3,0,0,s), state(0,0,3,3,e), Path).
dfs_algorithm(Starting, Goal, Path):-
	sub_dfs([[Starting]], Goal, Path).

% bfs_algorithm(state(3,3,0,0,s), state(0,0,3,3,e), Path).
bfs_algorithm(Starting, Goal, Path):-
	sub_bfs([[Starting]], Goal, Path).


