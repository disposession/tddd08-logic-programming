


makelist(nil, []).
makelist(n(Value, Subtree1, Subtree2), Sol):-
	makelist(Subtree1, Sol1),
	makelist(Subtree2, Sol2),
	append(Sol1, [Value], T),
	append(T, Sol2, Sol).


replace(_,_,0,[],[]).
replace(E,EE,1,[E|X],[EE|X]).
replace(E,EE,N,[E|T],Sol):-
	N1 is N - 1,
	replace(E,EE,N1,T,Sol1),
	append([EE],Sol1,Sol).
replace(E,EE,N,[X|T],Sol):-
	E\=X,
	replace(E,EE,N,T,Sol1),
	append([X],Sol1,Sol).